public class PessoaJurifica extends Pessoa {
    public String cnpj;
    public String nomeFantasia;
    public PessoaJurifica(String _nome, String _cnpj, String _nomeFantasia){
        super(_nome);
        this.cnpj = _cnpj;
        this.nomeFantasia = _nomeFantasia;
    }
}
