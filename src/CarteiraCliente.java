import java.util.ArrayList;
import java.util.List;

public class CarteiraCliente {
    public List<PessoaFisica> carteiraPF = new ArrayList<PessoaFisica>();
    public List<PessoaJurifica> carteiraPJ = new ArrayList<PessoaJurifica>();

    public List<PessoaFisica> getCarteiraPF() {
        return carteiraPF;
    }

    public void inserePessoaFisica(PessoaFisica pessoa){
        carteiraPF.add(pessoa);
    }

    public void inserePessoaJuridica(PessoaJurifica pessoa){
        carteiraPJ.add(pessoa);
    }

    public PessoaFisica getPessoaFisica(String cpf){
        PessoaFisica pf = null;
        for(int i=0;i<carteiraPF.size();i++){
            if(carteiraPF.get(i).cpf.equals(cpf)){
                pf = (PessoaFisica) carteiraPF.get(i);
            }
        }
        return pf;
    }

    public PessoaJurifica getPessoaJuridica(String cnpj){
        PessoaJurifica pj = null;
        for(int i=0;i<carteiraPJ.size();i++){
            if(carteiraPJ.get(i).cnpj.equals(cnpj)){
                pj = (PessoaJurifica) carteiraPJ.get(i);
            }
        }
        return pj;
    }
}
